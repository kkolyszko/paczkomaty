import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <div class="menu">
      <a href="prices">ceny</a>
      <a href="machines">obsługa paczkomatów</a>
      <a href="id">generator id</a>
    </div>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'paczkomaty';
}
