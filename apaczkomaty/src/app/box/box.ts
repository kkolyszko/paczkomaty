import { PackSize } from '../pack/pack-size';
import { Pack } from '../pack/pack';
import { MachineVisitor } from '../machine/machine-visitor';

export class Box {
    private pack?: Pack;
    constructor(private _size: PackSize) {}

    isEmpty(): boolean {
        return !this.pack;
    }

    put(pack: Pack) {
        this.pack = pack;
    }

    retrieve(): Pack {
        const pack = this.pack;
        this.pack = undefined;
        return pack;
    }

    get size(): PackSize {
        return this._size;
    }

    accept(visitor: MachineVisitor): void {
        visitor.visitBox(this);
    }
}