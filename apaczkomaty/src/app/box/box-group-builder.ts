import { Injectable } from '@angular/core';
import { BoxGroup } from './box-group';
import { PackSize } from '../pack/pack-size';
import { Box } from './box';

@Injectable()
export class BoxGroupBuilder {
    private aBoxes: number = 0;
    private bBoxes: number = 0;
    private cBoxes: number = 0;

    constructor() {}

    withA(aBoxes: number): BoxGroupBuilder {
        this.aBoxes = aBoxes;
        return this;
    }

    withB(bBoxes: number): BoxGroupBuilder {
        this.bBoxes = bBoxes;
        return this;
    }
    
    withC(cBoxes: number): BoxGroupBuilder {
        this.cBoxes = cBoxes;
        return this;
    }

    build(): BoxGroup {
        const group = new BoxGroup();

        group.boxes = this.buildBoxArray(PackSize.A, this.aBoxes)
        .concat(this.buildBoxArray(PackSize.B, this.bBoxes))
        .concat(this.buildBoxArray(PackSize.C, this.cBoxes));

        return group;
    }

    private buildBoxArray(packSize: PackSize, numberOfBoxes: number): Box[] {
        const array = [];
        for (let i = 0; i < numberOfBoxes; i++) {
            array.push(new Box(packSize));
        }
        return array;
    }
}