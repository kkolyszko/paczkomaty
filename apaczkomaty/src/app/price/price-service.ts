import { Injectable } from '@angular/core';
import { PackSize } from '../pack/pack-size';
import { PriceStrategyFactory } from './price-strategy-factory';

@Injectable()
export class PriceService {

    constructor(private priceStrategyFactory: PriceStrategyFactory) { }

    price(): number {
        return 10;
    }

    setPackSize(packSize: PackSize) {
    }

}