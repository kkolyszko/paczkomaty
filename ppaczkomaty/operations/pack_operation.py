from abc import ABCMeta, abstractmethod


class PackOperation(metaclass=ABCMeta):
    """ formal interface """

    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, "execute") and callable(subclass.execute)

    @abstractmethod
    def execute(self) -> None:
        raise NotImplementedError
