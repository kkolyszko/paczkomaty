from .pack_operation import PackOperation


def getBoxByPackId(packId, packMachine):
    for boxGroup in packMachine.boxGroups:
        for box in boxGroup.boxes:
            if box.isEmpty() is False and box.pack.id == packId:
                return box


class RetrievePackOperation(PackOperation):

    def __init__(self, packMachine, packId):
        self.packMachine = packMachine
        self.packId = packId

    def execute(self):
        # 1. znalezc box w ktorym jest paczka z id = packId
        box = getBoxByPackId(self.packId, self.packMachine)
        # 2. na boxie wykonac metode retrieve()
        return box.retrieve()
