from .pack_operation import PackOperation


class SendPackOperation(PackOperation):

    def __init__(self, packMachine, pack):
        self.packMachine = packMachine
        self.pack = pack

    def execute(self):
        self.packMachine.put(self.pack)
