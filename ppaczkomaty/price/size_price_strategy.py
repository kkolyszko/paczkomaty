from abc import ABCMeta, abstractmethod

from operations.pack_operation import PackOperation


class SizePriceStrategy(metaclass=ABCMeta):
    """ formal interface """

    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, "price") and callable(subclass.price)

    @abstractmethod
    def price(self) -> int:
        raise NotImplementedError
