from abc import ABCMeta, abstractmethod


class IdGenerator(metaclass=ABCMeta):
    """ formal interface """

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "getPackId")
            and callable(subclass.getPackId)
            and hasattr(subclass, "getPackMachineId")
            and callable(subclass.getPackMachineId)
            and hasattr(subclass, "getTransportId")
            and callable(subclass.getTransportId)
        )

    @abstractmethod
    def getPackId(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def getPackMachineId(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def getTransportId(self) -> str:
        raise NotImplementedError
