# Paczkomaty
The application is written from scratch with Python 3.x and unittest. The project does not expose real functionality, all code examples can be ran with tests.

## Tests
Tests can be run with shell command: `run_tests.sh` or simply by calling python directly, for example: `python -m unittest tests/box_test.py`

## HOWTO do more formal OOP in Python

Python is very flexible language, however it is possible to enforce more strict
behavior (like in e.g. Java) when it comes to interfaces and abstract classes.
On the other hand using type hints allow easier refactoring and much more.

### Formal interfaces

By using formal interface we ensure that exception will be raised if not all
methods defined in interface are implemented.

* more info about formal interfaces: https://realpython.com/python-interface/#formal-interfaces

### Formal abstract classes

By using formal abstract class we ensure that exception will be raised on class
instantiation.

* more info about formal abstract classes: https://realpython.com/inheritance-composition-python/#abstract-base-classes-in-python

### Type hints

Although Python is dynamically typed language it is possible to do off-line
type checking (still no type checking happens at runtime). By using type hints
it's ia. easier to do static analysis or code refactor.

* more info about type hints: https://www.python.org/dev/peps/pep-0484/
